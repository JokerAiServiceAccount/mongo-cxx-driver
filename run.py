from pathlib import Path
import itertools

paths_hpp = Path('./').glob('**/*.hpp')
paths_hh = Path('./').glob('**/*.hh')
paths_cpp = Path('./').glob('**/*.cpp')

for filepath in itertools.chain(paths_hpp, paths_cpp, paths_hh):
	content = []
	with open(filepath, "r") as fin:
		for line in fin:
			if (line.startswith('#include')):
				line = line.replace('<', '"')
				line = line.replace('>', '"')
			content.append(line)
	with open(filepath, "w") as fout:
		fout.writelines(content)

